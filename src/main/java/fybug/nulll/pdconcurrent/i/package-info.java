/**
 * <h2>接口包.</h2>
 * 提供基础功能接口与部分功能框架实现
 *
 * @author fybug
 * @version 0.0.1
 * @since PDConcurrent 0.1.2
 */
package fybug.nulll.pdconcurrent.i;